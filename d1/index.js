console.log("Hello World!");

/*Syntax of console.log: console.log(VariableOrStringToBeConsoled)*/

// It will comment parts of the code that gets ignored by the program. Single Line Comment

/*
	Multiple Line Comment
*/

///// [Section] Syntax, statements
/*  - Statements in programming  are instructions that we tell the computer to perform
 	- JS Statements usually end with semicolon(;), though not required but we will use it to help us train to locate where statement ends

 	- A syntax in programming , it is the set of rules that we describe statements must be constructed
	- All lines/blocks of code should be written in specific manner to work. This is due to how these codes where initially programmed to function in certain manner. */

///// [Section] Variable
/*
	- Variables are used to contain data
	- Any info that is used by an application is stored in what we call the memory
	- when we create variables, certain portions of device's memory is given a name that we call variables
	- This makes it easier for us to associate information  stored in our devices to actual "names" info
	
	DECLARING VARIABLE

	Syntax

	let const variable;
*/

let myVariable = "Jenno Vea";
console.log(myVariable);

// const keyword is use when a value of variable never change.
const constVariable = "James Bond";

//undefined
let Variable;
console.log(Variable); 
/*
	Guides in writing variables
	1. use the let keyword followed the variable name of your choose and use the assignment operator(=) to assign value.
	2. Variable names should start with lowercase character, use camelCasing for the multiple words.
	3. for constant variables, use the "const" keyword - fixed variable.
	4. Variable names should be indicative(descriptive) of the value being stored to avoid confusion.
*/

/*
	Declare and Initialize
	- Initializing variables - the instance when a variable is given  its first/initial value
		syntax :
			let/const variableName = initial vale;
*/

// Declation and Initialization of variable occur

productName = "desktop computer";
console.log(productName);

// Declaration
let desktopName;

console.log(desktopName)
// Initialization of the value of variable
desktopName = "Dell"
console.log(desktopName);

// Reassigning value
productName = "Personal Computer"
console.log(productName);

const name = "jenno";
console.log(name);


let lastName;
console.log(lastName);


	// Using var, bad practice.

 batch = "Batch 241"

var batch;
console.log(batch);

//////////////////////
/*
	let/const local/global scope

	scope essentially means where these variables are available for use

	let/const are block scope

	a block is a chuck of code bounded by {}. 
*/

/*let outerVariable = "hello";
{
	let innerVariable = "hello again";
	console.log(innerVariable);
}
console.log(outerVariable);*/

var outerVariable = "Hello";
{
	var innerVariable = "Hello again";
}

console.log(outerVariable);
console.log(innerVariable);

///// MULTIPLE VARIABLE ///////

let productCode = "DC017", productBrand = "Dell";
console.log(productCode);
console.log(productBrand);

///// Using a variable with a reserved keyword /////
// const let = "hello";
// console.log(let);


// [SECTION] Data Types

//String
/*
	- String are series of characters that create a word/phrase/sentence or anything related to creating text
	- Strings in javaScript can be written using either a single quote ('') or double quote (" ")
*/

let country = "Philippines";
let province = 'Metro Manila';
console.log(country);
console.log(province);

// Concatenate Strings
/*
	Multiple string values can be combined to create a single string using the + symbol
*/

let fullAddress = province + ", " + country;
console.log(fullAddress);

let greeting = "I live in the " + country;
console.log(greeting);

// Declaring a string using an escape character
let message = 'John\'s employees went home early.';
console.log(message);

message = "John\'s employees went home early";
console.log(message);

// "\n" - newline
let mailAddress = "Metro Manila\n\nPhilippines"
console.log(mailAddress);

// String
let wew = "wewwew";
console.log(typeof wew);

// Numbers
/*
	Integer/Whole Number
*/

// Whole/ Decimal
let headCount = 27, grade = 90.111;
console.log(typeof headCount);
console.log(typeof grade);

// Exponential Notation
let planetDistance = 2e10;
console.log(planetDistance);

// Combining text and string
console.log("John's grade last quarter is " + grade)

// Boolean
/*
	- normally used to store values relating to the state of certain things (true/false)
*/
let isMarried = false;
let isGoodConduct = true;
console.log( typeof isMarried);
console.log(isGoodConduct);

// Arrays
/*
	- special kind of data type that is used to store multiple values
Syntax:
	let/const arrayName = [elementA, elementB ...];
*/
// similar data type
let grades = [98, 92.1, 90.1, 94.7];
console.log(grades);

// Array is a special kind of object (not recommended)
let details = ["John", 25, true];
console.log(typeof details);

// Objects
/*
	it is used to mimic real world objects/items
	Syntax:
		let/const objectName = {
			propertyA: value,
			propertyB: value
		}
*/

let person = {
	firstName: "John",
	lastName: "Smith",
	age: "32",
	isMarried: false,
	contact: ["+639123456789", "1234-5678"],
	address: {
		houseNumber: "345",
		city: "Manila"
	}
}
console.log(person)

let myGrades = {
	firstGrading: 98,
	secondGrading: 92.1,
	thirdGrading: 90.1,
	fouthGrading: 94.7
}
console.log(myGrades)

/*
	Constant Object and Constant Arrays
	- we can change the element of an array to a constant variable
*/
const anime = ["one piece", "one punch man", "your lie in April"];
console.log(anime);
anime[0] = ["kimetsu no yaba"];
console.log(anime);

/*const anime1 = ["one piece", "one punch man", "your lie in April"]
console.log(anime1);
anime1 = ["kimetsu no yaba"];
console.log(anime1);*/

/*
	Null
	- used to intentionally express the absence of a value in a variable declaration/initialization
*/
let spouse = null;
console.log(typeof spouse);

// Undefined
// Represent the state of variable that has been declared but without an assigned value

let fullName;
console.log(typeof fullName);

let inARelationship;
console.log(inARelationship)